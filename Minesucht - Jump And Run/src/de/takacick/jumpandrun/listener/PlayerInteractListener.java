package de.takacick.jumpandrun.listener;

import java.util.Date;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import de.takacick.jumpandrun.JumpAndRun;
import de.takacick.jumpandrun.utils.JaR;
import de.takacick.jumpandrun.utils.Jumper;

public class PlayerInteractListener implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		// the player triggering the event
		Player player = event.getPlayer();

		// checks if the player is in setup mode
		if (JumpAndRun.getInstance().getJarHandler().getSetup().containsKey(player.getUniqueId())) {
			// checks if the player is interacting with a right click
			if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
					|| event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
				// checks if the players item in hand does exist
				if (player.getItemInHand() != null) {
					// checks if the players item in hand has a itemmeta
					if (player.getItemInHand().hasItemMeta()) {
						// checks if the players item in hand has a displayname
						if (player.getItemInHand().getItemMeta().getDisplayName() != null) {
							JaR jumpAndRun = JumpAndRun.getInstance().getJarHandler().getSetup()
									.get(player.getUniqueId());

							// checks if the players item in hand displayname contains a string
							if (player.getItemInHand().getItemMeta().getDisplayName().contains("Jump&Run-Spawn")) {
								if (event.getClickedBlock().getType().equals(Material.GOLD_PLATE)) {
									// updating the spawn location for this jump and run
									Location startLocation = event.getClickedBlock().getLocation();
									jumpAndRun.setStartLocation(startLocation);
									player.sendMessage(JumpAndRun.getInstance().getPrefix()
											+ "§7Du hast erfolgreich den §aJump&Run-Spawn §7festgelegt.");
								}
								// checks if the players item in hand displayname contains a string
							} else if (player.getItemInHand().getItemMeta().getDisplayName()
									.contains("Jump&Run-Ende")) {
								if (event.getClickedBlock().getType().equals(Material.GOLD_PLATE)) {
									// updating the end location for this jump and run
									Location endLocation = event.getClickedBlock().getLocation();
									jumpAndRun.setEndLocation(endLocation);
									player.sendMessage(JumpAndRun.getInstance().getPrefix()
											+ "§7Du hast erfolgreich das §cJump&Run-Ende §7festgelegt.");
								}
							} else if (player.getItemInHand().getItemMeta().getDisplayName()
									.contains("§7Markiere einen §eCheckpoint")) {
								if (event.getClickedBlock().getType().equals(Material.GOLD_PLATE)) {
									// updating the end location for this jump and run
									Location checkpointLocation = event.getClickedBlock().getLocation();
									if (jumpAndRun.getCheckpoint(checkpointLocation) == null) {
										player.sendMessage(JumpAndRun.getInstance().getPrefix()
												+ "§7Du hast erfolgreich einen §aCheckpoint §7hinzugefügt.");
										jumpAndRun.getCheckpoints().add(checkpointLocation);
									} else {
										player.sendMessage(JumpAndRun.getInstance().getPrefix()
												+ "§7Der Checkpoint ist bereits eingespeichert!");
									}
								}
							} else if (player.getItemInHand().getItemMeta().getDisplayName().contains("abbrechen")) {
								player.sendMessage(JumpAndRun.getInstance().getPrefix()
										+ "§7Du hast das Einstellen des §cJump&Runs §7abgebrochen.");
								// removing the player from the setup mode and settings his
								// inventory to his start inventory
								player.getInventory().clear();
								player.getInventory().setContents(JumpAndRun.getInstance().getJarHandler()
										.getInventories().get(player.getUniqueId()));
								player.updateInventory();
								JumpAndRun.getInstance().getJarHandler().getInventories().remove(player.getUniqueId());
								JumpAndRun.getInstance().getJarHandler().getSetup().remove(player.getUniqueId());
							} else if (player.getItemInHand().getItemMeta().getDisplayName().contains("speichern")) {
								player.sendMessage(JumpAndRun.getInstance().getPrefix()
										+ "§7Du hast das Jump&Run erfolgreich gespeichert!");
								player.sendMessage(JumpAndRun.getInstance().getPrefix()
										+ "§7Das Jump&Run ist nach einem Restart verfügbar!");

								// saving the jump and run
								JumpAndRun.getInstance().getJaRConfig().saveJumpAndRuns(
										JumpAndRun.getInstance().getJarHandler().getSetup().get(player.getUniqueId()));

								// removing the player from the setup mode and settings his
								// inventory to his start inventory
								player.getInventory().clear();
								player.getInventory().setContents(JumpAndRun.getInstance().getJarHandler()
										.getInventories().get(player.getUniqueId()));
								player.updateInventory();
								JumpAndRun.getInstance().getJarHandler().getInventories().remove(player.getUniqueId());
								JumpAndRun.getInstance().getJarHandler().getSetup().remove(player.getUniqueId());
							} else if (player.getItemInHand().getItemMeta().getDisplayName()
									.contains("§7Checkpoints löschen")) {
								player.sendMessage(JumpAndRun.getInstance().getPrefix()
										+ "§7Du hast erfolgreich alle Checkpoints gelöscht.");
								jumpAndRun.getCheckpoints().clear();
							}
						}
					}
				}
			}
			// checks if the player is interacting with a physical action
		} else if (event.getAction().equals(Action.PHYSICAL)) {

			// checks if the player is interacting with a gold_plate
			if (event.getClickedBlock().getType().equals(Material.GOLD_PLATE)) {
				// checks if the player is already in a jump and run
				if (!JumpAndRun.getInstance().getJarHandler().getJumper().contains(player)) {
					JaR jar = JumpAndRun.getInstance().getJarHandler()
							.getJumpAndRun(event.getClickedBlock().getLocation());
					// checks if the jump and run exists
					if (jar != null) {
						jar.getJumpers().add(new Jumper(player, jar));
						JumpAndRun.getInstance().getJarHandler().getJumper().add(player);
						player.sendMessage(JumpAndRun.getInstance().getPrefix() + "§aJump&Run gestartet! Viel Erfolg!");
					}
				} else {
					// getting the jump and run from the player
					JaR jar = JumpAndRun.getInstance().getJarHandler().getJumpAndRun(player);
					// checks if the player is at the end of the jump and run
					if (jar.getEndLocation().distance(event.getClickedBlock().getLocation()) <= 0.2) {
						// removing the player from the jump and run
						Jumper jumper = jar.getJumper(player);
						jar.getJumpers().remove(jar.getJumper(player));
						JumpAndRun.getInstance().getJarHandler().getJumper().remove(player);

						int secondsNeeded = new Date(jumper.getDuration()).getSeconds();
						player.sendMessage(JumpAndRun.getInstance().getPrefix() + "§aJump&Run beendet! Gut gemacht!");
						player.sendMessage(JumpAndRun.getInstance().getPrefix() + "§aDu hast §a" + secondsNeeded + "."
								+ (new Date(jumper.getDuration()).getTime() - secondsNeeded * 1000)
								+ " Sekunden benötigt!");
					} else {
						Location checkpoint = jar.getCheckpoint(event.getClickedBlock().getLocation());
						// checks if the checkpoint exists
						if (checkpoint != null) {
							Jumper jumper = jar.getJumper(player);
							// checks if this checkpoint is the next checkpoint
							if (jumper.getCheckpointId() < jar.getCheckpoints().indexOf(checkpoint)) {
								player.sendMessage(JumpAndRun.getInstance().getPrefix()
										+ "§aDu hast erfolgreich einen Checkpoint erreicht!");
								// updating the checkpoint
								jumper.setCheckpoint(player.getLocation());
								jumper.setCheckpointId(jar.getCheckpoints().indexOf(checkpoint));
							}
						}
					}
				}
			}
		}
	}
}


package de.takacick.jumpandrun.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.takacick.jumpandrun.JumpAndRun;

public class JaRHandler {

	// instance variables
	private ArrayList<JaR> jumpAndRuns;
	private ArrayList<Player> jumper;
	private HashMap<UUID, JaR> setup;
	private HashMap<UUID, ItemStack[]> inventories;

	/**
	 * Initializing this class
	 */
	public JaRHandler() {
		// Initializing instance variables
		jumpAndRuns = JumpAndRun.getInstance().getJaRConfig().getJumpAndRuns();
		setup = new HashMap<>();
		inventories = new HashMap<>();
		jumper = new ArrayList<>();
	}

	/**
	 * @param location which might be the start of a JaR
	 * @return the JaR or null
	 */
	public JaR getJumpAndRun(Location location) {
		for (JaR jar : jumpAndRuns) {
			// checks if the startlocation is the same as the location
			if (jar.getStartLocation().distance(location) <= 0.5d) return jar;
		}
		return null;
	}

	/**
	 * @param name which might be the name of a JaR
	 * @return the JaR or null
	 */
	public JaR getJumpAndRun(String name) {
		for (JaR jar : jumpAndRuns) {
			// checks if the j&r name equals the searched name
			if (name.equalsIgnoreCase(jar.getName())) return jar;
		}
		return null;
	}

	/**
	 * @param player which might competing this JaR
	 * @return the JaR or null
	 */
	public JaR getJumpAndRun(Player player) {
		for (JaR jar : jumpAndRuns) {
			// checks if the j&r inherits this player
			if (jar.getJumper(player) != null) return jar;
		}
		return null;
	}

	/**
	 * @return a HashMap which inherits all uuids which are setting up a jump and
	 *         run
	 */
	public HashMap<UUID, JaR> getSetup() {
		return setup;
	}

	/**
	 * @return a HashMap which inherits all uuids which are setting up a jump and
	 *         run
	 */
	public HashMap<UUID, ItemStack[]> getInventories() {
		return inventories;
	}

	/**
	 * @return a List inherits all player which are competing a j&r
	 */
	public ArrayList<Player> getJumper() {
		return jumper;
	}
}

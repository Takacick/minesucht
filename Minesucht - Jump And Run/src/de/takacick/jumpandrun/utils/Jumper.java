package de.takacick.jumpandrun.utils;

import java.util.Date;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Jumper {
	
	// instance variables
	private JaR jar;
	private Player player;
	private Long startTimeMillis;
	private Location checkpoint;
	private int checkpointId;

	/**
	 * @param player the jumper
	 * @param jar the jump and run
	 */
	public Jumper(Player player, JaR jar) {
		// Initializing instance variables
		this.jar = jar;
		this.player = player;
		this.startTimeMillis = new Date().getTime();
		this.checkpoint = player.getLocation();
		this.checkpointId = -1;
	}
	
	/**
	 * @return the jump and run for this player
	 */
	public JaR getJumpAndRun() {
		return jar;
	}
	
	/**
	 * @return the player object of this jumper
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * @return the duration for completing this jump and run
	 */
	public Long getDuration() {
		return new Date().getTime()-startTimeMillis;
	}
	
	/**
	 * @return last checkpoint location
	 */
	public Location getCheckpointLocation() {
		return checkpoint;
	}
	
	/**
	 * @return id for the last checkpoint
	 */
	public int getCheckpointId() {
		return checkpointId;
	}
	
	/**
	 * @param id for the current checkpoint
	 */
	public void setCheckpointId(int id) {
		this.checkpointId = id;
	}
	
	/**
	 * @param checkpoint new checkpoint location
	 */
	public void setCheckpoint(Location checkpoint) {
		this.checkpoint = checkpoint;
	}
}

package de.takacick.jumpandrun.utils;

public enum Difficulty {

	// all existing difficulties for different types of jump and runs
	HARD, MEDIUM, EASY;
}

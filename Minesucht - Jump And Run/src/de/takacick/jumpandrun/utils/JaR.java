package de.takacick.jumpandrun.utils;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class JaR {
	
	// instance variables
	private String name;
	private Location startLocation;
	private Location endLocation;
	private ArrayList<Location> checkpoints;
	private ArrayList<Jumper> jumper;
	private Difficulty difficulty;
	
	public JaR(String name) {
		// initialising instance variables
		this.name = name;
		this.difficulty = Difficulty.EASY;
		checkpoints = new ArrayList<>();
		jumper = new ArrayList<>();
	}
	
	/**
	 * @return the name for this JaR
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the start location for this JaR
	 */
	public Location getStartLocation() {
		return startLocation;
	}
	
	/**
	 * @return the end location for this JaR
	 */
	public Location getEndLocation() {
		return endLocation;
	}
	
	/**
	 * @param location the start location for this JaR
	 * @return this class
	 */
	public JaR setStartLocation(Location location) {
		this.startLocation = location;
		return this;
	}
	
	/**
	 * @param location the end location for this JaR
	 * @return this class
	 */
	public JaR setEndLocation(Location location) {
		this.endLocation = location;
		return this;
	}
	
	
	/**
	 * @return the all checkpoints                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
	 */
	public ArrayList<Location> getCheckpoints() {
		return checkpoints;
	}
	
	/**
	 * @param checkpoint location might be a checkpoint
	 * @return the checkpoint location or null
	 */
	public Location getCheckpoint(Location checkpoint) {
		//iterating throw the checkpoints ArrayList
		for(Location location : checkpoints) {
			//checking if the checkpoint location is 
			//is closely the same as location
			if(location.distance(checkpoint) <= 0.5) {
				return location;
			}
		}
		return null;
	}
	
	/**
	 * @return the difficulty of this JaR
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}
	
	/**
	 * @return a List inherits all player which are competing this j&r
	 */
	public ArrayList<Jumper> getJumpers() {
		return jumper;
	}
	
	/**
	 * @param player the player which will be searched for
	 * @return the jumper object of this player or null
	 */
	public Jumper getJumper(Player player) {
		for(Jumper jumper : jumper) {
			if(jumper.getPlayer().equals(player)) {
				return jumper;
			}
		}
		return null;
	}
	
	/**
	 * @param difficulty jar difficulty
	 * @return this class
	 */
	public JaR setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
		return this;
	}
	
	
	/**
	 * @return a copy of this class
	 */
	public JaR clone(){
		// cloning this jumpandrun class
		JaR jumpAndRun = new JaR(name).setDifficulty(difficulty).setEndLocation(endLocation).setStartLocation(startLocation);
		jumpAndRun.getCheckpoints().addAll(checkpoints);
		return jumpAndRun;
	}
}


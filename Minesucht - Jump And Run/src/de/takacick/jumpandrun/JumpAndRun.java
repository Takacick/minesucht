package de.takacick.jumpandrun;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.takacick.jumpandrun.commands.CheckpointCommand;
import de.takacick.jumpandrun.commands.ParkourCommand;
import de.takacick.jumpandrun.commands.SetupCommand;
import de.takacick.jumpandrun.config.JaRConfig;
import de.takacick.jumpandrun.listener.PlayerInteractListener;
import de.takacick.jumpandrun.utils.JaRHandler;

public class JumpAndRun extends JavaPlugin {
	
	// declaring instance variables
	private static JumpAndRun jumpAndRun;
	private JaRHandler jarHandler;	
	private JaRConfig jarConfig;

	private String prefix = "§8┃ §e§lJump&Run §8┃ ";
	private String noPermission = this.prefix + "§cDazu hast du keine Rechte.";

	
	/**
	 * on server start
	 */
	public void onEnable() {
		// initialize instance variables
		jumpAndRun = this;
		this.jarConfig = new JaRConfig();
		this.jarHandler = new JaRHandler();
				
		// registering commands and listeners
		Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
		Bukkit.getPluginCommand("setup").setExecutor(new SetupCommand());
		Bukkit.getPluginCommand("parkour").setExecutor(new ParkourCommand());
		Bukkit.getPluginCommand("checkpoint").setExecutor(new CheckpointCommand());
	}
	
	/**
	 * on server shutdown
	 */
	public void onDisable() {
		
	}
	
	/**
	 * @return this class
	 */
	public static JumpAndRun getInstance() {
		return jumpAndRun;
	}
	
	/**
	 * @return the jarhandler
	 */
	public JaRHandler getJarHandler() {
		return jarHandler;
	}
	
	/**
	 * @return the jardatabase
	 */
	public JaRConfig getJaRConfig() {
		return jarConfig;
	}
	
	/**
	 * @return the prefix string
	 */
	public String getPrefix() {
		return prefix;
	}
	
	/**
	 * @return the nopermission string
	 */
	public String getNoPermission() {
		return noPermission;
	}
}


package de.takacick.jumpandrun.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.takacick.jumpandrun.utils.Difficulty;
import de.takacick.jumpandrun.utils.JaR;

public class JaRConfig {

	private File file;
	private final String filePath = "plugins/JumpAndRun/jumpandruns.yml";
	private FileConfiguration config;

	/**
	 * constructor of this class and creates the jumpandruns.yml if not exist
	 */
	public JaRConfig() {
		file = new File(filePath);
		config = YamlConfiguration.loadConfiguration(file);

		// create file if not exists
		if (!file.exists()) {
			try {
				config.save(filePath);
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}
	}

	/**
	 * @return a list with all registered JaR
	 */
	public ArrayList<JaR> getJumpAndRuns() {
		ArrayList<JaR> jumpAndRuns = new ArrayList<>();

		config.getConfigurationSection("JumpAndRuns").getKeys(false).forEach(name -> {
			JaR jumpAndRun = new JaR(config.getString("JumpAndRuns." + name + ".name"))
					.setStartLocation((Location) config.get("JumpAndRuns." + name + ".startlocation"))
					.setEndLocation((Location) config.get("JumpAndRuns." + name + ".endlocation"))
					.setDifficulty(Difficulty.valueOf(config.getString("JumpAndRuns." + name + ".difficulty")));

			config.getConfigurationSection("JumpAndRuns." + name + ".checkpoint").getKeys(false).forEach(checkpoint -> {
				jumpAndRun.getCheckpoints()
						.add((Location) config.get("JumpAndRuns." + name + ".checkpoint." + checkpoint));
			});

			jumpAndRuns.add(jumpAndRun);
		});
		return jumpAndRuns;
	}

	/**
	 * @return a list with all registered JaR
	 */
	public void saveJumpAndRuns(JaR jumpandrun) {
		config.set("JumpAndRuns." + jumpandrun.getName() + ".name", jumpandrun.getName());
		config.set("JumpAndRuns." + jumpandrun.getName() + ".startlocation", jumpandrun.getStartLocation());
		config.set("JumpAndRuns." + jumpandrun.getName() + ".endlocation", jumpandrun.getEndLocation());

		jumpandrun.getCheckpoints().stream().forEach(checkpoint -> {
			config.set("JumpAndRuns." + jumpandrun.getName() + ".checkpoint."
					+ jumpandrun.getCheckpoints().indexOf(checkpoint), checkpoint);
		});

		config.set("JumpAndRuns." + jumpandrun.getName() + ".difficulty", jumpandrun.getDifficulty().name());

		try {
			config.save(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

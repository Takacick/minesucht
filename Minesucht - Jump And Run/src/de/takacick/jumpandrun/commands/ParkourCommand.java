package de.takacick.jumpandrun.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.takacick.jumpandrun.JumpAndRun;
import de.takacick.jumpandrun.utils.JaR;

public class ParkourCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// checks if the sender is a player
		if (sender instanceof Player) {
			// initialize and declare the player object
			Player player = (Player) sender;
			// checks if the arguments length is 1
			if (args.length == 1) {
				// checks if the first argument equals leave
				if (args[0].equalsIgnoreCase("leave")) {
					JaR jumpAndRun = JumpAndRun.getInstance().getJarHandler().getJumpAndRun(player);
					if (jumpAndRun != null) {
						player.sendMessage(JumpAndRun.getInstance().getPrefix() + "§cDu hast dein Jump&Run verlassen!");
						// removing the player from the jump and run
						jumpAndRun.getJumpers().remove(jumpAndRun.getJumper(player));
						JumpAndRun.getInstance().getJarHandler().getJumper().remove(player);
					} else {
						player.sendMessage(
								JumpAndRun.getInstance().getPrefix() + "§cDu bist momentan in keinem Jump&Run!");
					}
					return true;
				}
			}

			sendHelp(player);
		} else {
			sender.sendMessage("§cDieser Befehl ist nur für Spieler zugänglich");
		}
		return false;
	}

	/**
	 * sends a help message
	 * @param player which receives this messages
	 */
	private void sendHelp(Player player) {
		player.sendMessage("");
		player.sendMessage("§8§m--------§8┃ §e§lJump&Run §8§m┃---------");
		player.sendMessage("§8┃» §8/§7parkour §8- §7zeigt dir diese Hilfe");
		player.sendMessage("§8┃» §8/§7parkour leave §8- §7Verlasse deinen aktuellen Parkour");
		player.sendMessage("§8┃» §8/§7checkpoint §8- §7Teleportiere dich zu deinem Checkpoint");
		if (player.hasPermission("JaR.Setup")) {
			player.sendMessage("§8┃» /§7setup §8- §7Erstelle einen neuen Parkour");
		}
		player.sendMessage("§8§m--------§8┃ §e§lJump&Run §8§m┃---------");
		player.sendMessage("");
	}
}

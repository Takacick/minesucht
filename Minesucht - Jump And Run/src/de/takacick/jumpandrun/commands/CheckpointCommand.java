package de.takacick.jumpandrun.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.takacick.jumpandrun.JumpAndRun;


public class CheckpointCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// checks if the sender is a player
		if (sender instanceof Player) {
			// initialize and declare the player object
			Player player = (Player) sender;
			// checks if the player is in a jump and run
			if(JumpAndRun.getInstance().getJarHandler().getJumper().contains(player)) {
				// teleporting the player to his last checkpoint
				player.teleport(JumpAndRun.getInstance().getJarHandler().getJumpAndRun(player).getJumper(player).getCheckpointLocation());
				player.sendMessage(
						JumpAndRun.getInstance().getPrefix() + "§aDu bist nun bei deinem letzten Checkpoint!");
			} else {
				player.sendMessage(
						JumpAndRun.getInstance().getPrefix() + "§cDu bist momentan in keinem Jump&Run!");
			}
		} else {
			sender.sendMessage("§cDieser Befehl ist nur für Spieler zugänglich");
		}
		return false;
	}	
}

package de.takacick.jumpandrun.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.takacick.jumpandrun.JumpAndRun;
import de.takacick.jumpandrun.api.ItemBuilder;
import de.takacick.jumpandrun.utils.JaR;

public class SetupCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// checks if the sender is a player
		if (sender instanceof Player) {
			// initialize and declare the player object
			Player player = (Player) sender;
			// checks if the player has the needed permission
			if (player.hasPermission("JaR.Setup")) {
				// checks if the arguments length is 1
				if (args.length == 1) {
					// checks if the player is not already setting up a jump and run
					if (!JumpAndRun.getInstance().getJarHandler().getSetup().containsKey(player.getUniqueId())) {
						String jumpAndRunName = args[0];
						JaR jumpAndRun = JumpAndRun.getInstance().getJarHandler().getJumpAndRun(jumpAndRunName);
						// checks if the jump and run already exists
						if (jumpAndRun == null) {
							// registering the player in the setup map
							JumpAndRun.getInstance().getJarHandler().getSetup().put(player.getUniqueId(),
									new JaR(jumpAndRunName));
							player.sendMessage(
									JumpAndRun.getInstance().getPrefix() + "§7Das §aJump&Run §7wird erstellt!");
							player.sendMessage(
									JumpAndRun.getInstance().getPrefix() + "§7Du hast die nötigen Items erhalten.");
							player.sendMessage(
									JumpAndRun.getInstance().getPrefix() + "§7um das §aJump&Run §7einzustellen.");
						} else {
							JumpAndRun.getInstance().getJarHandler().getSetup().put(player.getUniqueId(),
									jumpAndRun.clone());
							player.sendMessage(
									JumpAndRun.getInstance().getPrefix() + "§7Dieses §aJump&Run §7existiert bereits.");
							player.sendMessage(JumpAndRun.getInstance().getPrefix()
									+ "§7Du hast jedoch die nötigen Items erhalten,");
							player.sendMessage(
									JumpAndRun.getInstance().getPrefix() + "§7um Änderungen daran vorzunehmen.");
						}
						// saving the player inventory
						JumpAndRun.getInstance().getJarHandler().getInventories().put(player.getUniqueId(),
								player.getInventory().getContents());
						// giving setup items
						giveSetupItems(player);
					}
				} else {
					player.sendMessage(JumpAndRun.getInstance().getPrefix() + "§7Verwende §8/§7setup §8<§7Jump&Run§8>");
				}
			} else {
				player.sendMessage(JumpAndRun.getInstance().getNoPermission());
			}
		} else {
			sender.sendMessage("§cDieser Befehl ist nur für Spieler zugänglich");
		}
		return false;
	}

	/**
	 * distributes the setup items to the given player
	 *
	 * @param player which gets the setup items
	 */
	private void giveSetupItems(Player player) {
		player.getInventory().clear();
		player.getInventory()
				.addItem(new ItemBuilder(Material.STICK).setName("§7Markiere den §aJump&Run-Spawn")
						.setLore("§8§m-------------", "§7Klicke mit diesem Stick auf eine goldene Druckplatte,",
								"§7um den §aJump&Run-Spawn §7festzulegen!")
						.toItemStack());

		player.getInventory()
				.addItem(new ItemBuilder(Material.STICK).setName("§7Markiere einen §eCheckpoint")
						.setLore("§8§m-------------", "§7Klicke mit diesem Stick auf eine eiserne Druckplatte,",
								"§7um den §eCheckpoint §afestzulegen!")
						.toItemStack());

		player.getInventory()
				.addItem(new ItemBuilder(Material.STICK).setName("§7Markiere das §cJump&Run-Ende")
						.setLore("§8§m-------------", "§7Klicke mit diesem Stick auf eine goldene Druckplatte,",
								"§7um das §cJump&Run-Ende §7festzulegen!")
						.toItemStack());

		player.getInventory()
				.addItem(new ItemBuilder(Material.EMERALD).setName("§7Jump&Run speichern")
						.setLore("§8§m-------------", "§7Interagiere mit diesem Item,", "§7um das J&R zu speichern!")
						.toItemStack());

		player.getInventory()
				.addItem(new ItemBuilder(Material.REDSTONE).setName("§7Jump&Run Setup abbrechen")
						.setLore("§8§m-------------", "§7Interagiere mit diesem Item,", "§7um den Vorgang abzubrechen!")
						.toItemStack());

		player.getInventory().addItem(new ItemBuilder(Material.REDSTONE).setName("§7Checkpoints löschen")
				.setLore("§8§m-------------", "§7Interagiere mit diesem Item,", "§7um alle Checkpoints zu löschen!")
				.toItemStack());
	}
}
